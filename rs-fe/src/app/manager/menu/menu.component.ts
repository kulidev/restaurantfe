import {Component, OnInit, ViewChild} from '@angular/core';
import {CreateMenuRequest, ManagerService} from '../../shared/services/manager.service';
import {Meal} from '../../shared/models/Meal';
import {HomeService} from '../../shared/services/home.service';
import {DailyMenu} from '../../shared/models/dailyMenu.model';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  @ViewChild('f', {static: false}) form: NgForm;
  today: string;
  meals: Meal[];
  dailyMenus: DailyMenu[];

  tmpMeals: Meal[] = [];
  idDailyMenu: number = null;
  constructor(private managerService: ManagerService, private homeService: HomeService) { }

  ngOnInit() {
    this.SetToday();

    this.managerService.getMeals().subscribe(response => {
      this.meals = response;
      console.log(this.meals);
    });

    this.homeService.getFollowingDailyMenu().subscribe(response => {
      this.dailyMenus = response.dailyMenus;
      console.log(this.dailyMenus);
    });
  }

  AddMeal(meal: Meal) {
    if (!this.tmpMeals.find(x => x.meailId === meal.meailId)) {
      this.tmpMeals.push(meal);
    }
  }

  removeMeal(meal: Meal): void {
    this.tmpMeals = this.tmpMeals.filter(x => x.meailId !== meal.meailId);
  }

  Clear() {
    this.tmpMeals = [];
    this.SetToday();
    this.idDailyMenu = null;
  }

  showMenu(dailyMenu: DailyMenu) {
    this.idDailyMenu = dailyMenu.idDailyMenu;
    this.tmpMeals = dailyMenu.meals.slice();
  }


  onSubmit() {
    // nove menu
    if (this.idDailyMenu === null) {
      const request = new CreateMenuRequest();
      request.mealsId = this.tmpMeals.map(x => x.meailId);
      request.date = this.form.value.date;

      this.managerService.createMeals(request).subscribe(response => {
        console.log(response);
        this.homeService.getFollowingDailyMenu().subscribe(res => {
          this.dailyMenus = res.dailyMenus;
          console.log(this.dailyMenus);
        });
      });
      //uprava stavajiho menu
    } else {

    }
  }

  private SetToday() {
    const date = new Date();
    const month = date.getMonth() + 1 > 9 ? date.getMonth() + 1 : `0${date.getMonth() + 1}`;
    this.today = `${date.getFullYear()}-${month}-${date.getDate()}`;
  }

}
