import { Component, OnInit } from '@angular/core';
import {ManagerService} from '../../shared/services/manager.service';
import {ReservationRequest} from '../../shared/models/reservationRequest';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class ReservationsComponent implements OnInit {
  reservations: ReservationRequest[];
  constructor(private managerService: ManagerService) { }

  ngOnInit() {
    this.managerService.getReservation().subscribe(res => {
      this.reservations = res.reservationsReservationRequests;
      console.log(res);
      console.log(res.reservationsReservationRequests);
      console.log(this.reservations);
    });
  }

}
