import {Component, OnInit} from '@angular/core';
import {ManagerService} from '../../shared/services/manager.service';
import {GetOrder} from '../../shared/models/GetOrder';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders: GetOrder[];
  sumOfPrice: number = 0;
  sumOfMeal: number = 0;

  constructor(private managerService: ManagerService) {
  }

  ngOnInit() {
    this.managerService.getOrders().subscribe(res => {
      this.orders = res.orders;
      console.log(res);
      this.sumOfMeal = this.orders.filter(x => x.success === true).length;
      this.sumOfPrice = this.orders.filter(x => x.success === true).map(x => x.meal.price).reduce((a, b) => a + b, 0);
    });
  }

  onSuccess(id: number) {
    // this.managerService.UpdateOrder(id).subscribe(res => {
    //   console.log(res);
    //   this.managerService.getOrders().subscribe(response => {
    //     this.orders = response.orders;
    //     this.sumOfMeal = this.orders.filter(x => x.success === true).length;
    //     this.sumOfPrice = this.orders.filter(x => x.success === true).map(x => x.meal.price).reduce((a, b) => a + b, 0);
    //   });
    // });
  }
}
