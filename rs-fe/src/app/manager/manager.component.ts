import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Routes} from '@angular/router';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {
  @Output() logOut = new EventEmitter<boolean>();
  constructor(private route: ActivatedRoute) { }
  actualTab = 'menu';

  ngOnInit() {
  }

  onLogOut() {
    this.logOut.emit(false);
  }

  onTmpRoute() {
    console.log(this.route);
  }

  OnChangeTab(tab: string) {
    this.actualTab = tab;
  }

}
