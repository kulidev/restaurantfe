import {Component, Input, OnInit} from '@angular/core';
import {Meal} from '../../shared/models/Meal';

@Component({
  selector: 'app-daily-menu',
  templateUrl: './daily-menu.component.html',
  styleUrls: ['./daily-menu.component.css']
})
export class DailyMenuComponent implements OnInit {

  @Input() dailyMeal: Meal;

  constructor() { }

  ngOnInit() {
  }

}
