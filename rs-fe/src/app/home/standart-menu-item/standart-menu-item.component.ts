import {Component, Input, OnInit} from '@angular/core';
import {Meal} from '../../shared/models/Meal';

@Component({
  selector: 'app-standart-menu-item',
  templateUrl: './standart-menu-item.component.html',
  styleUrls: ['./standart-menu-item.component.css']
})
export class StandartMenuItemComponent implements OnInit {

  @Input() meal: Meal;

  constructor() { }

  ngOnInit() {
  }

}
