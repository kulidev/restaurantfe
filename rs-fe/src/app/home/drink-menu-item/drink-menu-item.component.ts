import {Component, Input, OnInit} from '@angular/core';
import {Drink} from '../../shared/models/drink.model';

@Component({
  selector: 'app-drink-menu-item',
  templateUrl: './drink-menu-item.component.html',
  styleUrls: ['./drink-menu-item.component.css']
})
export class DrinkMenuItemComponent implements OnInit {

  @Input() drink: Drink;

  constructor() { }

  ngOnInit() {
  }

}
