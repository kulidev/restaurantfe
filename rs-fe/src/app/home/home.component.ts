import { Component, OnInit } from '@angular/core';
import {HomeService} from '../shared/services/home.service';
import {Meal} from '../shared/models/Meal';
import {Drink} from '../shared/models/drink.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  mealMenu = true;
  dailyMeals: Meal[];
  standartMeals: Meal[];
  drinks: Drink[];

  names: string[] = ['Špagety carbonara', 'dada', 'daadda'];

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.homeService.getHomeData().subscribe(response => {
      this.dailyMeals = response.dailyMeals;
      this.standartMeals = response.standartMeals;
      this.drinks = response.drinks;
      console.log(response);
    });
  }

  onChangeMealMenu() {
    this.mealMenu = true;
  }

  onChangeDrinkMenu() {
    this.mealMenu = false;
  }

}
