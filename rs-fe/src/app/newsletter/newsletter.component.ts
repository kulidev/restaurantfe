import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {HomeService} from '../shared/services/home.service';


@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.css']
})
export class NewsletterComponent implements OnInit {
  interval;
  notificationDisplay: boolean = false;
  notificationText: string;

  @ViewChild('f', {static: false}) form: NgForm;

  constructor(private homeService: HomeService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.homeService.addEmailToNewsletter(this.form.value.email).subscribe(response => {
      this.notificationText = response.message;
      this.notificationDisplay = true;
      this.interval = setTimeout(() => {
        this.notificationDisplay = false;
      },
        3000);
    });

    this.form.reset();
  }
}
