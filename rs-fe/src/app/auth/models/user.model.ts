export class User {
  constructor(public  id: number, public email: string, private _token: string, private _tokenExpiration: Date, public roles?: string[], ) {
  }

  get token() {
    if (!this._tokenExpiration || new Date() > this._tokenExpiration) {
      return null;
    }
    return this._token;
  }
}
