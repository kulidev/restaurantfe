import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {
  loginDisplay: boolean = true;
  private userSub: Subscription;
  isAuthenticated = false;
  customerSection = false;
  employeeSection = false;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.userSub = this.authService.userSubject.subscribe(user => {
      this.isAuthenticated = user ? true : false;
      if (user) {
        this.customerSection = user.roles.includes('customer');
        this.employeeSection = !this.customerSection;
      }
    });
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }


  onChangeLogin() {
    this.loginDisplay = true;
  }

  onChangeRegistration() {
    this.loginDisplay = false;
  }

  onLogOut(state: boolean) {
    this.isAuthenticated = state;
  }

}
