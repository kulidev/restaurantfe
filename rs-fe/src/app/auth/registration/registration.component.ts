import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../shared/services/auth.service';
import {Customer} from '../../shared/models/customer.model';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  @ViewChild('f', {static: false}) form: NgForm;
  notificationDispley: boolean = false;
  message: string = '';
  isLoading = false;
  httpStatus: number;


  constructor(private userService: AuthService) {  }


  ngOnInit() {
  }

  onSubmit() {
    this.isLoading = true;
    const customer = new Customer();
    customer.firstName = this.form.value.firstName;
    customer.lastName = this.form.value.lastName;
    customer.email = this.form.value.email;
    customer.password = this.form.value.password;
    customer.specialOffer = this.form.value.customSwitch1 !== '';

    this.userService.addUser(customer).subscribe(response => {
      this.message = response.message;
      this.httpStatus = null;
      this.notificationDispley = true;
      this.isLoading = false;
      },
      error => {
        this.httpStatus = error.status;
        this.message = error.error.message;
        this.notificationDispley = true;
        this.isLoading = false;

      });
  }
}
