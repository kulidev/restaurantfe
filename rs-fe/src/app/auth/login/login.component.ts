import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Customer} from '../../shared/models/customer.model';
import {AuthService} from '../../shared/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('f', {static: false}) form: NgForm;
  isLoading = false;
  constructor(private userService: AuthService, private router: Router) { }
  displayWrongPass: boolean = false;
  displayWrongEmail: boolean = false;


  ngOnInit() {
  }

  onSubmit() {
    this.isLoading = true;
    const customer = new Customer();
    customer.email = this.form.value.email;
    customer.password = this.form.value.password;

    this.displayWrongPass = false;
    this.displayWrongEmail = false;

    this.userService.login(customer).subscribe(response => {
        this.isLoading = false;
        // if (!response.roles.includes('customer')) {
        //   this.router.navigate(['manager']);
        // }
      },
      error => {
        this.isLoading = false;
        if (error.status === 401) {
          this.displayWrongPass = true;
        }
        if (error.status === 400 && error.error.code === 'EMAIL_NOT_FOUND') {
          this.displayWrongEmail = true;
        }
      });


  }

}
