import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Meal} from '../../shared/models/Meal';

@Component({
  selector: 'app-order-daily-menu',
  templateUrl: './order-daily-menu.component.html',
  styleUrls: ['./order-daily-menu.component.css']
})
export class OrderDailyMenuComponent implements OnInit {
  @Input() dailyMeal: Meal;
  @Output() mealAddedEvent = new EventEmitter<Meal>();
  constructor() { }

  ngOnInit() {
  }

  AddMealToOrder() {
    // console.log(this.dailyMeal);
    this.mealAddedEvent.emit(this.dailyMeal);
    console.log('ted');
    console.log(this.dailyMeal);
  }

}
