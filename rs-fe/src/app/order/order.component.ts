import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {CustomerService, UpdateOrder} from '../shared/services/customer.service';
import {Order} from '../shared/models/order.model';
import {Meal} from '../shared/models/Meal';
import {HomeService} from '../shared/services/home.service';
import {DailyMenu} from '../shared/models/dailyMenu.model';
import {Router} from '@angular/router';
import {AuthService} from '../shared/services/auth.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  @Output() logOut = new EventEmitter<boolean>();
  dailyMeals: Meal[];
  orderedMeals: Meal[] = [];
  menus: DailyMenu[];
  orderId: number;
  takeaway: boolean;
  notificationDisplay: boolean = false;
  notificationText: string;
  interval;

  constructor(private customerService: CustomerService, private homeService: HomeService, private router: Router, private authService: AuthService) { }

  ngOnInit() {
    this.customerService.getOrder().subscribe(response => {
      console.log(response);
      this.orderedMeals = response.meals;
      this.orderId = response.orderId;
      this.takeaway = response.takeaway;
    });

    this.homeService.getFollowingDailyMenu().subscribe(response => {
      this.menus = response.dailyMenus;
      if (response) {
        this.dailyMeals = response.dailyMenus.shift().meals;
      }
    });

  }

  onUpdateOrder() {
    let updateOrder = new UpdateOrder();
    updateOrder.mealIds = this.orderedMeals.map(x => x.meailId);
    updateOrder.takeaway = this.takeaway;

    console.log(updateOrder);
    if (this.orderId !== 0) {
      updateOrder.orderId = this.orderId;
      this.customerService.updateOrder(updateOrder).subscribe(response => {
        console.log(response);
        this.notificationText = 'Objednávka byla upravena';
      });
    }
    else {
      updateOrder.mealIds = this.orderedMeals.map(x => x.meailId);
      this.customerService.createOrder(updateOrder).subscribe(response => {
        console.log(response);
        this.notificationText = 'Objednávka byla vytvořena';
      });
    }

    this.notificationDisplay = true;
    this.interval = setTimeout(() => {
        this.notificationDisplay = false;
      },
      3000);
  }

  onAddMealToOrder(meal: Meal) {
    if (!this.orderedMeals) {
      this.orderedMeals = [];
    }
    this.orderedMeals.push(meal);
  }

  deleteMeal(index: number) {
    this.orderedMeals.splice(index, 1);
  }

  getDate() {

    let date = new Date();
    date.setDate(date.getDate() + 1);
    const today = `${date.getDate()}.${date.getMonth() + 1}.`;
    return today;
  }

  onLogOut() {
    this.logOut.emit(false);
  }


}
