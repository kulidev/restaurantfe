import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ReservationComponent} from './reservation/reservation.component';
import {OrderComponent} from './order/order.component';
import {ManagerComponent} from './manager/manager.component';
import {AuthComponent} from './auth/auth.component';
import {NewsletterComponent} from './newsletter/newsletter.component';
import {MealComponent} from './manager/meal/meal.component';
import {MenuComponent} from './manager/menu/menu.component';
import {DrinkComponent} from './manager/drink/drink.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'reservation', component: ReservationComponent},
  {path: 'newsletter', component: NewsletterComponent},
  {path: 'orders', component: OrderComponent},

  // {path: 'manager', component: ManagerComponent, children: [
  //       {path: 'meal', component: MealComponent},
  //       {path: 'menu', component: MenuComponent},
  //       {path: 'drink', component: DrinkComponent}
  //     ]},
  {path: 'auth', component: AuthComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
