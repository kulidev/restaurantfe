import {Component, OnInit, ViewChild} from '@angular/core';
import {ReservationRequest, ReservationService} from '../shared/services/reservation.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {
  today: string;
  reservationResult: string;
  notificationDispley: boolean = false;
  interval;

  @ViewChild('f', {static: false}) form: NgForm;

  constructor(private reservationService: ReservationService) { }

  ngOnInit() {
    const date = new Date();
    const month = date.getMonth() + 1 > 9 ? date.getMonth() + 1 : `0${date.getMonth() + 1}`;
    this.today = `${date.getFullYear()}-${month}-${date.getDate()}`;
  }

  onSubmit() {
    const request = new ReservationRequest();
    request.onName = this.form.value.name;
    request.countPeople = +this.form.value.countPeople;
    request.email = this.form.value.email;
    request.startTime = +this.form.value.startTime;
    request.finishTime = +this.form.value.startTime + +this.form.value.spentTime;
    request.date = this.form.value.date;

    console.log(request);

    this.reservationService.addReservation(request).subscribe(response => {
      console.log(response);
      switch (response.result) {
        case 'success': {
          this.reservationResult = 'Vaše rezervace proběhla úspěšně.';
          break;
        }
        case 'fail': {
          this.reservationResult = 'Bohužel restaurace je v tuto dobu plně obsazená';
          break;
        }
        case 'needCall': {
          this.reservationResult = 'Pro vyšší počet lidí a soukromé akce děláme rezervace pouze na zavolání';
          break;
        }
      }
      this.notificationDispley = true;
      // this.interval = setTimeout(() => {
      //     this.notificationDispley = false;
      //   },
      //   3000);
    });
  }
}
