import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { ReservationComponent } from './reservation/reservation.component';
import { ManagerComponent } from './manager/manager.component';
import { OrderComponent } from './order/order.component';
import { AuthComponent } from './auth/auth.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import { NewsletterComponent } from './newsletter/newsletter.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { DailyMenuComponent } from './home/daily-menu/daily-menu.component';
import { StandartMenuItemComponent } from './home/standart-menu-item/standart-menu-item.component';
import { DrinkMenuItemComponent } from './home/drink-menu-item/drink-menu-item.component';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { LoadingSpinnerComponent } from './shared/loading-spinner/loading-spinner.component';
import { OrderDailyMenuComponent } from './order/order-daily-menu/order-daily-menu.component';
import { ReviewComponent } from './home/review/review.component';
import { MealComponent } from './manager/meal/meal.component';
import { MenuComponent } from './manager/menu/menu.component';
import { DrinkComponent } from './manager/drink/drink.component';
import { OrdersComponent } from './manager/orders/orders.component';
import { ReservationsComponent } from './manager/reservations/reservations.component';
import {TranslateDayNamePipe} from './shared/pipes/translateDayName.pipe';
import {TranslateTimeIntToTimePipePipe} from './shared/pipes/TransalateTimeIntToTime.pipe';
import {TranslateDateIntToDatePipe} from './shared/pipes/translateDateIntToDate.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ReservationComponent,
    ManagerComponent,
    OrderComponent,
    AuthComponent,
    NewsletterComponent,
    DailyMenuComponent,
    StandartMenuItemComponent,
    DrinkMenuItemComponent,
    LoginComponent,
    RegistrationComponent,
    LoadingSpinnerComponent,
    OrderDailyMenuComponent,
    ReviewComponent,
    MealComponent,
    MenuComponent,
    DrinkComponent,
    OrdersComponent,
    ReservationsComponent,
    TranslateDayNamePipe,
    TranslateTimeIntToTimePipePipe,
    TranslateDateIntToDatePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
