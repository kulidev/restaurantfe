import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'translateDay'
})
export class TranslateDayNamePipe implements PipeTransform {
  transform(day: string): string {
    console.log(day);
    switch (day) {
      case 'Monday': {
        return 'Pondělí';
      }
      case 'Tuesday': {
        return 'Úterý';
      }
      case 'Wednesday': {
        return 'Středa';
      }
      case 'Thursday': {
        return 'Čtvrtek';
      }
      case 'Friday': {
        return 'Pátek';
      }
      case 'Saturday': {
        return 'Sobota';
      }
      case 'Sunday': {
        return 'Neděle';
      }
    }
  }
}
