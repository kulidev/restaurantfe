import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'TranslateTimeIntToTimePipe'
})
export class TranslateTimeIntToTimePipePipe implements PipeTransform {
  transform(value: number): string {
    return value.toString().slice(0, 2) + ':' + value.toString().slice(2, 4);
  }
}

