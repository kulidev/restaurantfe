import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'TranslateDateIntToDatePipe'
})
export class TranslateDateIntToDatePipe implements PipeTransform {
  transform(value: number): string {
    return value.toString().slice(7, 8) + '/' + value.toString().slice(5, 6) + '/' + value.toString().slice(0, 4);
  }
}
