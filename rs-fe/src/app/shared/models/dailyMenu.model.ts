import {Meal} from './Meal';

export class DailyMenu {
  meals: Meal[];
  date: Date;
  idDailyMenu: number;
}
