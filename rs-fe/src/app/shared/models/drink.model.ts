export class Drink {
  name: string;
  price: number;
  portion: string;
  drinkCode: string;
}
