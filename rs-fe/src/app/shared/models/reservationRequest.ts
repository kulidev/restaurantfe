export class ReservationRequest {
  onName: string;
  countPeople: number;
  startTime: number;
  finishTime: number;
  date: Date;
  email: string;
  table: number;
  capacity: number;
  dateInt: number;
}
