import {Meal} from './Meal';

export class GetOrder {
  orderId: number;
  takeaway: boolean;
  success: boolean;
  customerName: string;
  customerId: number;
  meal: Meal;
}
