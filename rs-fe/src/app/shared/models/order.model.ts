import {Meal} from './Meal';

export class Order {
  orderId: number;
  takeaway: boolean;
  meals: Meal[];
}
