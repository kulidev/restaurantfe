export class Meal {
  meailId: number;
  name: string;
  price: number;
  portion: string;
  calories: number;
  allergens: string;
  description: string;
  image: string;
}
