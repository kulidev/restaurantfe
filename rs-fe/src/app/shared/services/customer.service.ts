import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AuthService} from './auth.service';
import {User} from '../../auth/models/user.model';
import {take} from 'rxjs/operators';
import {Order} from '../models/order.model';

export class UpdateOrder {
  mealIds: number[];
  orderId: number;
  takeaway: boolean;
}

@Injectable({providedIn: 'root'})
export class CustomerService {
  constructor(private httpClient: HttpClient, private authService: AuthService) {}
  user: User;

  getOrder() {
    this.authService.userSubject.pipe(take(1)).subscribe(user => {
      this.user = user;
    });

    const params = new HttpParams()
    .set('customerId', this.user.id.toString())
    .set('email', this.user.email);

    return this.httpClient.get<Order>('http://localhost:56847/api/order', {
      params,
      headers: new HttpHeaders({Authorization: 'Bearer ' + this.user.token}),
    });
  }

  createOrder(requestBody: UpdateOrder) {
    this.authService.userSubject.pipe(take(1)).subscribe(user => {
      this.user = user;
    });

    const params = new HttpParams()
      .set('customerId', this.user.id.toString())
      .set('email', this.user.email);

    return this.httpClient.post<UpdateOrder>('http://localhost:56847/api/order', requestBody,{
      params,
      headers: new HttpHeaders({Authorization: 'Bearer ' + this.user.token}),
    });
  }

  updateOrder(requestBody: UpdateOrder) {
    this.authService.userSubject.pipe(take(1)).subscribe(user => {
      this.user = user;
    });

    const params = new HttpParams()
      .set('customerId', this.user.id.toString())
      .set('email', this.user.email);

    return this.httpClient.put<number[]>('http://localhost:56847/api/order', requestBody, {
      params,
      headers: new HttpHeaders({Authorization: 'Bearer ' + this.user.token}),
    });
  }

  deleteOrder(orderId: number) {
    this.authService.userSubject.pipe(take(1)).subscribe(user => {
      this.user = user;
    });

    const params = new HttpParams()
      .set('customerId', this.user.id.toString())
      .set('orderId', orderId.toString());

    return this.httpClient.delete<any>('http://localhost:56847/api/order', {
      params,
      headers: new HttpHeaders({Authorization: 'Bearer ' + this.user.token}),
    });
  }
}
