import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

export class ReservationRequest {
  onName: string;
  countPeople: number;
  startTime: number;
  finishTime: number;
  date: Date;
  email: string;
}

export interface ReservationResponse {
  result: string;
}


@Injectable({providedIn: 'root'})
export class ReservationService {
  constructor(private httpClient: HttpClient) {}

  addReservation(requestBody: ReservationRequest) {
    return  this.httpClient.post<ReservationResponse>('http://localhost:56847/api/Home', requestBody);
  }
}
