import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Meal} from '../models/Meal';
import {Drink} from '../models/drink.model';
import {DailyMenu} from '../models/dailyMenu.model';

export interface HomeNewsletterResponse {
  message: string;
}

export interface DailyMenuResponse {
  dailyMenus: DailyMenu[];
}

export class HomeNewsletterRequest {
  email: string;
}

export class HomeResponse {
  dailyMeals: Meal[];
  standartMeals: Meal[];
  drinks: Drink[];
}

@Injectable({providedIn: 'root'})
export class HomeService {

  constructor(private httpClient: HttpClient) {}

  addEmailToNewsletter(email: string) {
    const mail = new HomeNewsletterRequest();
    mail.email = email;
    return  this.httpClient.post<HomeNewsletterResponse>('http://localhost:56847/api/Home/newsletter', mail);
  }

  getHomeData() {
    return this.httpClient.get<HomeResponse>('http://localhost:56847/api/Home');
  }

  getFollowingDailyMenu() {
    return this.httpClient.get<DailyMenuResponse>('http://localhost:56847/api/Home/other');
  }
}
