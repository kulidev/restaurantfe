import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Meal} from '../models/Meal';
import {AuthService} from './auth.service';
import {User} from '../../auth/models/user.model';
import {take} from 'rxjs/operators';
import {Order} from '../models/order.model';
import {ReservationRequest} from '../models/reservationRequest';
import {GetOrder} from '../models/GetOrder';

export interface MealResponse {
  meals: Meal[];
}

export class CreateMenuRequest {
  mealsId: number[];
  date: Date;
}

export class UpdateMenuRequest {
  mealsId: number[];
  date: Date;
  MenuId: number;
}

export interface GetReservationResponse {
  reservationsReservationRequests: ReservationRequest[];
}

export interface GetOrderResponse {
  orders: GetOrder[];
}

@Injectable({providedIn: 'root'})
export class ManagerService {

  constructor(private httpClient: HttpClient, private authService: AuthService) {}
    user: User;


  getMeals() {
    this.authService.userSubject.pipe(take(1)).subscribe(user => {
      this.user = user;
    });

    const params = new HttpParams()
      .set('email', this.user.email);

    return this.httpClient.get<Meal[]>('http://localhost:56847/api/manager/meals', {
      params,
      headers: new HttpHeaders({Authorization: 'Bearer ' + this.user.token}),
    });
  }

  createMeals(request: CreateMenuRequest) {
    this.authService.userSubject.pipe(take(1)).subscribe(user => {
      this.user = user;
    });

    const params = new HttpParams()
      .set('email', this.user.email);
    console.log(request);

    return this.httpClient.post<CreateMenuRequest>('http://localhost:56847/api/manager/menu', request, {
      params,
      headers: new HttpHeaders({Authorization: 'Bearer ' + this.user.token}),
    });
  }

  getReservation() {
    this.authService.userSubject.pipe(take(1)).subscribe(user => {
      this.user = user;
    });

    const params = new HttpParams()
      .set('email', this.user.email);

    return this.httpClient.get<GetReservationResponse>('http://localhost:56847/api/manager/reservation', {
      params,
      headers: new HttpHeaders({Authorization: 'Bearer ' + this.user.token}),
    });
  }

  getOrders() {
    this.authService.userSubject.pipe(take(1)).subscribe(user => {
      this.user = user;
    });

    const params = new HttpParams()
      .set('email', this.user.email);

    return this.httpClient.get<GetOrderResponse>('http://localhost:56847/api/manager/orders', {
      params,
      headers: new HttpHeaders({Authorization: 'Bearer ' + this.user.token}),
    });
  }

  UpdateOrder(id: number) {
    this.authService.userSubject.pipe(take(1)).subscribe(user => {
      this.user = user;
    });

    const params = new HttpParams()
      .set('email', this.user.email)
      .set('id', id.toString());

    return this.httpClient.put<any>('http://localhost:56847/api/manager/orders', {
      params,
      headers: new HttpHeaders({Authorization: 'Bearer ' + this.user.token}),
    });
  }
}
