import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Customer} from '../models/customer.model';
import {catchError, tap} from 'rxjs/operators';
import {BehaviorSubject, throwError} from 'rxjs';
import {User} from '../../auth/models/user.model';

export interface RegistrationResponse {
  result: string;
  message: string;
}

export interface LoginResponse {
  idToken: string;
  email: string;
  roles: string[];
  expiresIn: number;
  idUser: number;
}

@Injectable({providedIn: 'root'})
export class AuthService {
  userSubject = new BehaviorSubject<User>(null);


  constructor(private httpClient: HttpClient) {
  }

  addUser(requestBody: Customer) {
    return this.httpClient
      .post<RegistrationResponse>('http://localhost:56847/api/user/registration', requestBody)
      // .pipe(catchError(errorRes => {
      //     console.log(errorRes);
      //     const errorMessage = ' Nastala neznámá chyba';
      //     if (!errorRes.error.error) {
      //       return throwError(errorMessage);
      //     } else {
      //       return throwError('blablbalbl');
      //     }
      //   })
      // )
      ;
  }


  login(requestBody: Customer) {
    return this.httpClient
      .post<LoginResponse>('http://localhost:56847/api/user/login', requestBody)
      .pipe(catchError(this.handleError), tap(response => {
        const expirationDate = new Date(new Date().getTime() + +response.expiresIn * 1000);
        const user = new User(response.idUser, response.email, response.idToken, expirationDate, response.roles);
        this.userSubject.next(user);
      }));
  }

  private handleError(errorRes: HttpErrorResponse) {
    console.log(errorRes);

    // const errorMessage = 'Nastala neznámá chyba';
    return throwError(errorRes);
  }
}
